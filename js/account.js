/**
 * Confirm user wants to recreate store pages. Any changes to the content of the pages will be overwritten.
 */
function confirmPages()
{
  if (confirm('Any changes you have made to these pages will be overwritten. Would you like to proceed?'))
  {
    document.pages_form.submit();
  }
}

/**
** Add a listener to process messages posted back from our website.
*/
jQuery(document).ready(function() {

  function getData (e) {
    // Get JSON object.
    eval('var obj = ' + e.data);

    // Only process our messages.
    if (e.origin.indexOf("cashiecommerce.com") < 0) {
      return false;
    }
  }

  if (typeof window.addEventListener != 'undefined') {
    window.addEventListener('message', getData, false);
  }
  else if (typeof window.attachEvent != 'undefined') {
    window.attachEvent('onmessage', getData);
  }

})
