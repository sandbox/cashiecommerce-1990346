/**
 * Add a listener to process messages posted back from our website.
 */
jQuery(document).ready(function() {

  function getHash(e) {

    // Get JSON object
    eval('var obj = ' + e.data);

    // Only process our messages.
    if (e.origin.indexOf("cashiecommerce.com") < 0) {
      return false;
    }

    // Get the hash
    document.profile_form.hash.value = obj.hash;

    if (obj.details_dynamic == true) {
      document.profile_form.details_dynamic.value = 1;
    }
    else {
      document.profile_form.details_dynamic.value = 0;
    }

    // First time a user has logged in so store hash and create store pages.
    if ((document.profile_form.oldhash.value == null || document.profile_form.oldhash.value == '') && (document.profile_form.hash.value != document.profile_form.oldhash.value)) {
      document.profile_form.submit();
    }
    else if (document.profile_form.hash.value != document.profile_form.oldhash.value) {
      // Can't log in with different Cashie Commerce account.
      document.getElementById("iframe_dashboard").src = logoutURL;
      alert('You have already associated this Drupal site with a different Cashie Commerce account. De-activate and re-activate this module to use a different Cashie Commerce account.');
    }
  }

  if (typeof window.addEventListener != 'undefined') {
    window.addEventListener('message', getHash, false);
  }
  else if (typeof window.attachEvent != 'undefined') {
    window.attachEvent('onmessage', getHash);
  }

})
