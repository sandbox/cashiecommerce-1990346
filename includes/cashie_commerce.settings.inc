<?php
/**
 * @file
 * General settings page.
 */

/**
 * Menu hook for settings menu.
 */
function cashie_commerce_settings() {
  drupal_add_css(drupal_get_path('module', 'cashie_commerce') . '/css/page.css', 'module');
  return '<iframe src="' . CASHIE_COMMERCE_URL . '/account/settings?' . CASHIE_COMMERCE_URL_VARS . '&origin=' . urlencode(CASHIE_COMMERCE_ORIGIN) . '&returnURL=' . urlencode(CASHIE_COMMERCE_RETURN) . '" width="930" height="1800" frameborder="0" name="iframe_products" id="iframe_products"></iframe>';
}
