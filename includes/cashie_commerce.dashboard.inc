<?php

/**
 * @file
 * Account settings page.
 */

/**
 * Menu hook for dashboard menu.
 */
function cashie_commerce_dashboard() {

  // Process first time user has logged in with new Cashie Commerce account.
  if (isset($_POST['update_hash'])) {

    include_once 'cashie_commerce.utils.inc';

    // Create pages, also save pages & hash to db.
    cashie_commerce_create_pages($_POST['hash']);

    // Parse return URL so web service knows where to redirect.
    $pos = strpos(CASHIE_COMMERCE_RETURN, "?");
    if ($pos === FALSE) {
      $returnurl = CASHIE_COMMERCE_RETURN;
    }
    else {
      $returnurl = substr(CASHIE_COMMERCE_RETURN, 0, $pos);
    }

    // Post values to our web service.
    return '
      <form id="updateurl_form" name="updateurl_form" method="post" action="' . CASHIE_COMMERCE_URL . '/api/users/save_urls">
        <input type="hidden" name="cart"  value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CART], array('absolute' => TRUE)) . '" /> 
              <input type="hidden" name="checkout" value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CHECKOUT], array('absolute' => TRUE)) . '" />  
              <input type="hidden" name="success" value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_SUCCESS], array('absolute' => TRUE)) . '" />
              <input type="hidden" name="failure" value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_FAILURE], array('absolute' => TRUE)) . '" />
              <input type="hidden" name="catalog" value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CATALOG], array('absolute' => TRUE)) . '" /> 
              <input type="hidden" name="details" value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_DETAILS], array('absolute' => TRUE)) . '" />
              <input type="hidden" name="static_details" value="" />  
              <input type="hidden" name="returnURL" value="' . $returnurl . '?update=1" />          
      </form>   
          <script type="text/javascript">
        document.updateurl_form.submit();
      </script>
    ';
  }

  // See if we need to display status message.
  $notify = '';
  if (!empty($_GET['update'])) {
    return '<div class="alert alert-success">Your Drupal site has been linked to your Cashie Commerce account and your store pages have been created.</div>
      <iframe src="' . CASHIE_COMMERCE_URL . '/account/dashboard?' . CASHIE_COMMERCE_URL_VARS . '&origin=' . urlencode(CASHIE_COMMERCE_ORIGIN) . '&returnURL=' . urlencode(CASHIE_COMMERCE_RETURN) . '" width="930" height="2000" frameborder="0" name="iframe_dashboard" id="iframe_dashboard"></iframe>';
  }

  // Show the account page.
  if (empty($GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_HASH])) {
    $iframe = '<iframe src="' . CASHIE_COMMERCE_URL . '/sign_up?' . CASHIE_COMMERCE_URL_VARS . '&origin=' . urlencode(CASHIE_COMMERCE_ORIGIN) . '&returnURL=' . urlencode(CASHIE_COMMERCE_RETURN) . '" width="930" height="2000" frameborder="0" name="iframe_dashboard" id="iframe_dashboard"></iframe>';
  }
  else {
    $iframe = '<iframe src="' . CASHIE_COMMERCE_URL . '/login?' . CASHIE_COMMERCE_URL_VARS . '&origin=' . urlencode(CASHIE_COMMERCE_ORIGIN) . '&returnURL=' . urlencode(CASHIE_COMMERCE_RETURN) . '" width="930" height="1000" frameborder="0" name="iframe_dashboard" id="iframe_dashboard"></iframe>';
  }

  drupal_add_css(drupal_get_path('module', 'cashie_commerce') . '/css/page.css', 'module');
  drupal_add_js(drupal_get_path('module', 'cashie_commerce') . '/js/dashboard.js');

  return '
    <script type="text/javascript">
    var logoutURL = "' . CASHIE_COMMERCE_URL . '/logout?' . CASHIE_COMMERCE_URL_VARS . '&origin=' . urlencode(CASHIE_COMMERCE_ORIGIN) . '&returnURL=' . urlencode(CASHIE_COMMERCE_RETURN) . '";
    </script>
    ' . $notify . $iframe . '
    <form id="profile_form" name="profile_form" method="post" action="">
      <input type="hidden" name="hash"  value="" /> 
      <input type="hidden" name="oldhash" value="' . (empty($GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_HASH]) ? '' : $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_HASH]) . '" />  
      <input type="hidden" name="details_dynamic" value="1" />
      <input type="hidden" name="update_hash"  value="true" />           
    </form>
    <script>document.profile_form.action = window.location.href;</script>
  ';
}
