<?php
/**
 * @file
 * Design settings page.
 */

/**
 * Menu hook for design menu.
 */
function cashie_commerce_design() {
  drupal_add_css(drupal_get_path('module', 'cashie_commerce') . '/css/page.css', 'module');
  return '<iframe src="' . CASHIE_COMMERCE_URL . '/account/design?' . CASHIE_COMMERCE_URL_VARS . '&origin=' . urlencode(CASHIE_COMMERCE_ORIGIN) . '&returnURL=' . urlencode(CASHIE_COMMERCE_RETURN) . '" width="930" height="1200" frameborder="0" name="iframe_design" id="iframe_design"></iframe>';
}
