<?php
/**
 * @file
 * Help page.
 */

/**
 * Menu hook for help menu.
 */
function cashie_commerce_help() {
  drupal_add_css(drupal_get_path('module', 'cashie_commerce') . '/css/page.css', 'module');
  return '<iframe src="' . CASHIE_COMMERCE_URL . '/support?' . CASHIE_COMMERCE_URL_VARS . '&origin=' . urlencode(CASHIE_COMMERCE_ORIGIN) . '&returnURL=' . urlencode(CASHIE_COMMERCE_RETURN) . '" width="930" height="1200" frameborder="0" name="iframe_help" id="iframe_help"></iframe>';
}
