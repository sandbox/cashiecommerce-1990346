<?php
/**
 * @file
 * Utility functions
 */

/**
 * Create the six store pages required by Cashie Commerce.
 */
function cashie_commerce_create_pages($hash = '') {

  $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_HASH] = $hash;

  // Shopping cart page.
  $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CART] = cashie_commerce_create_node($GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CART], array(
    'title' => 'Shopping Cart',
    'summary' => 'Shopping Cart',
    'in_menu' => TRUE,
    'html' => '<script type="text/javascript">document.write(unescape("%3Cscript src=\'" + (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + "' . CASHIE_COMMERCE_S3 . '.s3.amazonaws.com/userjs/' . $hash . '-cart.js\' type=\'text/javascript\'%3E%3C/script%3E"));</script>',
  ));

  // Checkout page.
  $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CHECKOUT] = cashie_commerce_create_node($GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CHECKOUT], array(
    'title' => 'Checkout',
    'summary' => 'Checkout',
    'in_menu' => FALSE,
    'html' => '<script type="text/javascript">document.write(unescape("%3Cscript src=\'" + (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + "' . CASHIE_COMMERCE_S3 . '.s3.amazonaws.com/userjs/' . $hash . '-checkout.js\' type=\'text/javascript\'%3E%3C/script%3E"));</script>',
  ));

  // Order success page.
  $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_SUCCESS] = cashie_commerce_create_node($GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_SUCCESS], array(
    'title' => 'Order Success',
    'summary' => 'Order Success',
    'in_menu' => FALSE,
    'html' => '<script type="text/javascript">document.write(unescape("%3Cscript src=\'" + (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + "' . CASHIE_COMMERCE_S3 . '.s3.amazonaws.com/js/response-success.js\' type=\'text/javascript\'%3E%3C/script%3E"));</script>',
  ));

  // Order failure page.
  $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_FAILURE] = cashie_commerce_create_node($GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_FAILURE], array(
    'title' => 'Order Failed',
    'summary' => 'Order Failed',
    'in_menu' => FALSE,
    'html' => '<script type="text/javascript">document.write(unescape("%3Cscript src=\'" + (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + "' . CASHIE_COMMERCE_S3 . '.s3.amazonaws.com/js/response-failure.js\' type=\'text/javascript\'%3E%3C/script%3E"));</script>',
  ));

  // Product catalog page.
  $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CATALOG] = cashie_commerce_create_node($GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CATALOG], array(
    'title' => 'Products',
    'summary' => 'Products',
    'in_menu' => TRUE,
    'html' => '<script type="text/javascript">document.write(unescape("%3Cscript src=\'" + (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + "' . CASHIE_COMMERCE_S3 . '.s3.amazonaws.com/userjs/' . $hash . '-catalog.js\' type=\'text/javascript\'%3E%3C/script%3E"));</script>',
  ));

  // Product details page.
  $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_DETAILS] = cashie_commerce_create_node($GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_DETAILS], array(
    'title' => 'Product Details',
    'summary' => 'Product Details',
    'in_menu' => FALSE,
    'html' => '<script type="text/javascript">document.write(unescape("%3Cscript src=\'" + (\'https:\' == document.location.protocol ? \'https://\' : \'http://\') + "' . CASHIE_COMMERCE_S3 . '.s3.amazonaws.com/userjs/' . $hash . '-details.js\' type=\'text/javascript\'%3E%3C/script%3E"));</script>',
  ));

  // Save our config to the db.
  variable_set(CASHIE_COMMERCE_VARIABLE_OPTIONS, $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS]);
}

/**
 * Create a Drupal node (page).
 */
function cashie_commerce_create_node($cur_node = NULL, $options = array()) {

  global $user;

  // Set some default options.
  $options += array(
    'type' => 'page',
    'title' => 'Cashie',
    'description' => 'This is the description.',
    'summary' => 'This is the summary.',
    'in_menu' => TRUE,
    'html' => '',
  );

  // Update existing node.
  if (!empty($cur_node)) {
    // Try loading the node.
    $node = node_load($cur_node, NULL, TRUE);
    // Load failed so user may have deleted node.
    if (!$node) {
      // Set variable to force creation of a new node.
      $cur_node = FALSE;
    }
  }

  // Create a new node.
  if (empty($cur_node)) {
    $node = new stdClass();
    $node->type = $options['type'];
    $node->title = $options['title'];
    $node->language = LANGUAGE_NONE;
    $node->uid = $user->uid;
    node_object_prepare($node);
  }

  // Let's add standard body field.
  $node->body[$node->language][0]['value'] = $options['html'];
  $node->body[$node->language][0]['summary'] = $options['summary'];
  // Format is required since we are inserting script tags.
  $node->body[$node->language][0]['format'] = 'full_html';
  // Prepare node for a submit.
  $node = node_submit($node);
  // Save & publish node, after this call we'll get a nid.
  node_save($node);

  // Only create menu links when creating new pages.
  if (empty($cur_node) && $options['in_menu']) {
    // Add to main top menu.
    $menu_item = array(
      'link_title' => $options['title'],
      'link_path'  => 'node/' . $node->nid,
      'menu_name'  => 'main-menu',
    );
    $mlid = menu_link_save($menu_item);
    // Add to Navigation menu.
    $menu_item = array(
      'link_title' => $options['title'],
      'link_path'  => 'node/' . $node->nid,
      'menu_name'  => 'navigation',
    );
    $mlid = menu_link_save($menu_item);
  }

  // Return the node id.
  return $node->nid;
}
