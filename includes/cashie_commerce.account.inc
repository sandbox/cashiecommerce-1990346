<?php

/**
 * @file
 * Account settings page.
 */

/**
 * Menu hook for account menu.
 */
function cashie_commerce_account() {

  // Include our CSS and JS.
  drupal_add_css(drupal_get_path('module', 'cashie_commerce') . '/css/page.css', 'module');
  drupal_add_js(drupal_get_path('module', 'cashie_commerce') . '/js/account.js');

  // Process user request to recreate store pages.
  if (isset($_POST['create_pages'])) {

    include_once 'cashie_commerce.utils.inc';

    // Create pages, also save pages & hash to db.
    cashie_commerce_create_pages($GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_HASH]);

    // Parse return URL so web service knows where to redirect.
    $pos = strpos(CASHIE_COMMERCE_RETURN, "?");
    if ($pos === FALSE) {
      $returnurl = CASHIE_COMMERCE_RETURN;
    }
    else {
      $returnurl = substr(CASHIE_COMMERCE_RETURN, 0, $pos);
    }

    // Post values to our web service.
    return '
        <form id="updateurl_form" name="updateurl_form" method="post" action="' . CASHIE_COMMERCE_URL . '/api/users/save_urls">
            <input type="hidden" name="cart"  value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CART], array('absolute' => TRUE)) . '" /> 
            <input type="hidden" name="checkout" value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CHECKOUT], array('absolute' => TRUE)) . '" />  
            <input type="hidden" name="success" value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_SUCCESS], array('absolute' => TRUE)) . '" />
            <input type="hidden" name="failure" value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_FAILURE], array('absolute' => TRUE)) . '" />
            <input type="hidden" name="catalog" value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CATALOG], array('absolute' => TRUE)) . '" /> 
            <input type="hidden" name="details" value="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_DETAILS], array('absolute' => TRUE)) . '" />
            <input type="hidden" name="static_details" value="" />  
            <input type="hidden" name="returnURL" value="' . $returnurl . '?update=1" />          
        </form>   
        <script type="text/javascript">
            document.updateurl_form.submit();
        </script>
    ';
  }

  // See if we need to display status message.
  $notify = '';
  if (!empty($_GET['update'])) {
    $notify = '<div class="alert alert-success">Your store pages have been recreated.</div>';
  }

  // Show the account page.
  return $notify . '<iframe src="' . CASHIE_COMMERCE_URL . '/account/account?' . CASHIE_COMMERCE_URL_VARS . '&origin=' . urlencode(CASHIE_COMMERCE_ORIGIN) . '&returnURL=' . urlencode(CASHIE_COMMERCE_RETURN) . '" width="930" height="1200" frameborder="0" name="iframe_account" id="iframe_account"></iframe>
      <br />
      <link href="https://fonts.googleapis.com/css?family=Varela" rel="stylesheet" type="text/css" />
      <div style="border: solid 1px #DBDBBB;margin-top: 20px;padding: 20px;position: relative; width: 760px;">
      <h2 style="font-size: 14px; color: #333; margin: 0; padding: 0; line-height: 14px;">Recreate your store pages</h2>
      <div class="notification-box info">Cashie Commerce has created the following pages in your site: <a href="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CATALOG], array('absolute' => TRUE)) . '" target="_blank">Product Catalog</a>, <a href="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_DETAILS], array('absolute' => TRUE)) . '" target="_blank">Product Details</a>, <a href="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CART], array('absolute' => TRUE)) . '" target="_blank">Shopping Cart</a>, <a href="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_CHECKOUT], array('absolute' => TRUE)) . '" target="_blank">Checkout</a>, <a href="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_SUCCESS], array('absolute' => TRUE)) . '" target="_blank">Order Success</a>, <a href="' . url('node/' . $GLOBALS[CASHIE_COMMERCE_GLOBAL_OPTIONS][CASHIE_COMMERCE_VARIABLE_URL_FAILURE], array('absolute' => TRUE)) . '" target="_blank">Order Failure</a>. To ensure that our online store functions properly, you should not delete these pages or modify their URLs. If you need Cashie Commerce to recreate these pages, click the "Recreate Pages" button below.</div>
      <br />
      <form id="pages_form" name="pages_form" method="post" action="">
          <input type="hidden" name="create_pages"  value="true" />
          <input type="hidden" name="update"  value="true" />           
          <div class="buttons"><a class="button-action" onclick="javascript:confirmPages();" alt="Recreate your store pages if something is not working or you deleted one of the pages." title="Recreate your store pages if something is not working or you deleted one of the pages.">Recreate Pages</a></div>           
      </form>
      <script>document.pages_form.action = window.location.href;</script>
      </div>
  ';
}
