<?php
/**
 * @file
 * Orders page.
 */

/**
 * Menu hook for orders menu.
 */
function cashie_commerce_orders() {
  drupal_add_css(drupal_get_path('module', 'cashie_commerce') . '/css/page.css', 'module');
  return '<iframe src="' . CASHIE_COMMERCE_URL . '/account/transactions?' . CASHIE_COMMERCE_URL_VARS . '&origin=' . urlencode(CASHIE_COMMERCE_ORIGIN) . '&returnURL=' . urlencode(CASHIE_COMMERCE_RETURN) . '" width="930" height="1200" frameborder="0" name="iframe_orders" id="iframe_orders"></iframe>';
}
