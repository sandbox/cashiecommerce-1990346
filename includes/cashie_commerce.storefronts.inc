<?php
/**
 * @file
 * Storefronts page.
 */

/**
 * Menu hook for storefronts menu.
 */
function cashie_commerce_storefronts() {
  drupal_add_css(drupal_get_path('module', 'cashie_commerce') . '/css/page.css', 'module');
  return '<iframe src="' . CASHIE_COMMERCE_URL . '/account/storefronts?' . CASHIE_COMMERCE_URL_VARS . '&origin=' . urlencode(CASHIE_COMMERCE_ORIGIN) . '&returnURL=' . urlencode(CASHIE_COMMERCE_RETURN) . '" width="930" height="1800" frameborder="0" name="iframe_products" id="iframe_products"></iframe>';
}
