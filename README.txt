********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: Cashie Commerce Module
Author: Cashie Commerce <support at cashiecommerce dot edu>
Drupal: 7
********************************************************************

NOTE: 

Cashie Commerce is the easiest way to sell on any Drupal site. 
      
DESCRIPTION:

Cashie Commerce is the easiest way to create and run successful 
social, mobile & online stores. With Cashie Commerce, you can sell 
products, services and digital goods, provide a secure and reliable 
shopping cart and run promotions and discounts, all while managing 
everything from your Drupal admin.

Developers: Cashie Commerce’s ecommerce platform helps you maximize 
productivity, providing clients with an efficient shopping cart they 
can easily manage themselves.

Download the module and sign up for a free trial today to build and 
test your store.

********************************************************************

INSTALLATION:

1. Place the entire cashie_commerce directory into your Drupal modules
   directory (normally sites/all/modules or modules).

2. Enable the action module by navigating to:

     Administer > Modules

   When the module is enabled, you should see "Cashie Commerce" in the
   top level menu in your Drupal admin.

********************************************************************
